package dam.androidalejandroromero.u3_tasca1_layouts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements Button.OnClickListener {

    public static final String PRUEBA = "Prueba";

    Button bLogin, bPantalla1, bCalculadora, bTable, bGrid, bForm, bPeliculas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI() {
        bLogin = findViewById(R.id.bLogin);
        bPantalla1 = findViewById(R.id.bPantalla1);
        bCalculadora = findViewById(R.id.bCalculadora);
        bTable = findViewById(R.id.bTable);
        bGrid = findViewById(R.id.bGrid);
        bForm = findViewById(R.id.bForm);
        bPeliculas = findViewById(R.id.bPeliculas);

        bLogin.setOnClickListener(this);
        bPantalla1.setOnClickListener(this);
        bCalculadora.setOnClickListener(this);
        bTable.setOnClickListener(this);
        bGrid.setOnClickListener(this);
        bForm.setOnClickListener(this);
        bPeliculas.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.bLogin:
                Log.d(PRUEBA,"login");
                startActivity(new Intent(this, A1Login.class));
                break;
            case R.id.bPantalla1:
                Log.d(PRUEBA,"Pantalla1");
                startActivity(new Intent(this, A2Pantalla1.class));
                break;
            case R.id.bCalculadora:
                Log.d(PRUEBA, "Calculadora");
                startActivity(new Intent(this, A3Calculadora.class));
                break;
            case R.id.bTable:
                startActivity(new Intent(this, A4Table.class));
                break;
            case R.id.bGrid:
                startActivity(new Intent(this, A5GridLayout.class));
                break;
            case R.id.bForm:
                startActivity(new Intent(this, A6Form.class));
                break;
            case R.id.bPeliculas:
                startActivity(new Intent(this, A7Peliculas.class));
                break;
        }
    }

}