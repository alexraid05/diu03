package dam.androidalejandroromero.u3_tasca1_layouts;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class A3Calculadora extends AppCompatActivity implements View.OnClickListener {

    Button button37;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3_calculadora);

        setUI();
    }

    private void setUI() {
        button37= findViewById(R.id.button37);

        button37.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, MainActivity.class));
    }
}